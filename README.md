# Arch Linux PKGBUILD for updateDVB #


### What is this repository for? ###

PKGBUILD to build updateDVB package for Arch Linux from the
https://gitlab.com/updatelee/updateDVB git repo.

### How do I get set up? ###

Before you can compile and run this software you must install
the patched driver modules and DVB kernel headers for v4l-updatelee.
See...

https://gitlab.com/updatelee/v4l-updatelee

Alternatively you can compile and install the  kernel, modules 
and headers files by building Arch packages using this PKGBUILD....

https://bitbucket.org/bluzee/linux-udl

### Building the updateDVB Arch package ##

Dependencies are qt5-base qt5-serialport qwt qt5-svg mesa

Make dependency is git.

git clone https://bitbucket.org/bluzee/updatedvb-arch

cd updatedvb-arch

makepkg -si



updateDVB should now be installed.
